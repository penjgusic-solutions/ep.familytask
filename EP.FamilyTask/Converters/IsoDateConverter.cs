﻿using Newtonsoft.Json.Converters;

namespace EP.Nikolina_Marina.Converters
{
    public class GenesisDateConverter : IsoDateTimeConverter
    {
        public GenesisDateConverter()
        {
            base.DateTimeFormat = "yyyy-MM-dd'T'HH:mm:ss.fff'Z'";
        }
    }
}
