﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EP.Nikolina_Marina.Models.Address;

namespace EP.Nikolina_Marina.Interfaces
{
    public interface IAddressService
    {
        Task<List<AddressModel>> GetMembersAsync();

        Task<ExtendedAddreFields> GetMemberAsync(string id);

        Task UpdateMemberAsync(ExtendedAddreFields model);

        Task CreateMemberAsync(AddresFields model);

        Task DeleteMemberAsync(string id);
    }
}
