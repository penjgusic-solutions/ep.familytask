﻿using RestSharp;

namespace EP.Nikolina_Marina.Interfaces
{
    public interface IHttpClientFactory
    {
        RestClient GetRestClient();
    }
}