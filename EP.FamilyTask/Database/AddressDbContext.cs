﻿using EP.Nikolina_Marina.Models.Address;
using Microsoft.EntityFrameworkCore;

namespace EP.Nikolina_Marina.Database
{
    public class AddressDbContext : DbContext
    {
        public AddressDbContext(DbContextOptions<AddressDbContext> options) : base(options)
        {
        }

        public DbSet<AddresFields> Address { get; set; }
    }
}
