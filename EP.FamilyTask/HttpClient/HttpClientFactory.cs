﻿using System;
using System.Text;
using EP.Nikolina_Marina.Interfaces;
using EP.Nikolina_Marina.Settings;
using Microsoft.Extensions.Options;
using RestSharp;

namespace EP.Nikolina_Marina.HttpClient
{
    public class HttpClientFactory : IHttpClientFactory
    {
        private readonly GenesisSettings _settings;

        public HttpClientFactory(IOptions<GenesisSettings> settings)
        {
            _settings = settings.Value;
        }

        public RestClient GetRestClient()
        {
            var encoding = new UTF8Encoding();

            var client = new RestClient(_settings.Url);

            var bytes = encoding.GetBytes($"{_settings.Username}:{_settings.Password}");

            var authInfo = Convert.ToBase64String(bytes);

            client.AddDefaultHeader("Authorization", $"Basic {authInfo}");

            client.AddDefaultHeader("X-CAS-PRODUCT-KEY", _settings.ProductKey);

            client.Timeout = 15000;

            return client;
        }
    }
}