﻿namespace EP.Nikolina_Marina.Settings
{
    public class GenesisSettings
    {
        public string Url { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string ProductKey { get; set; }
    }
}
