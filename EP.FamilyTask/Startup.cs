﻿using System;
using AutoMapper;
using EP.Nikolina_Marina.Database;
using EP.Nikolina_Marina.HttpClient;
using EP.Nikolina_Marina.Implementation;
using EP.Nikolina_Marina.Interfaces;
using EP.Nikolina_Marina.Settings;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace EP.Nikolina_Marina
{
    public class Startup
    {
        private readonly IConfigurationSection _httpSettingsSection;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            _httpSettingsSection = configuration.GetSection(nameof(GenesisSettings));
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<GenesisSettings>(_httpSettingsSection);

            services.AddTransient<IHttpClientFactory, HttpClientFactory>();
            services.AddSingleton<IAddressService, AddressService>();

            services.AddDbContext<AddressDbContext>(options => options.UseInMemoryDatabase("FamilyTodoDb"));

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
