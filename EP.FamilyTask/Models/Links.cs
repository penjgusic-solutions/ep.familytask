﻿namespace EP.Nikolina_Marina.Models
{
    public class Links
    {
        public string Self { get; set; }

        public string Dossier { get; set; }
    }
}