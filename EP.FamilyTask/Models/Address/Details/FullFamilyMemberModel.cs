﻿namespace EP.Nikolina_Marina.Models.Address.Details
{
    public class FullFamilyMemberModel
    {
        public string ObjectType { get; set; }

        public string Id { get; set; }

        public FamilyMemberDetailsFields Fields { get; set; }

        public Links Links { get; set; }
    }
}
