﻿using System;

namespace EP.Nikolina_Marina.Models.Address.Details
{
    public class FamilyMemberDetailsFields
    {
        public string GWBRANCH { get; set; }
        public bool BIRTHDAYGREETINGS { get; set; }
        public int BLOCKED_EMAILS { get; set; }
        public int CBADDRESS { get; set; }
        public int CBFAX1 { get; set; }
        public int CBFAX2 { get; set; }
        public int CBFAX3 { get; set; }
        public int CBIM { get; set; }
        public int CBMAIL { get; set; }
        public int CBPHONE1 { get; set; }
        public int CBPHONE2 { get; set; }
        public int CBPHONE3 { get; set; }
        public int CBWEB { get; set; }
        public string CHRISTIANNAME { get; set; }
        public bool CHRISTMASGREETINGS { get; set; }
        public string COMPNAME { get; set; }
        public int FOREIGNEDITPERMISSION { get; set; }
        public string GEOCODESTATUS { get; set; }
        public int GEOCODESTATUSNUMBER { get; set; }
        public string GGUID { get; set; }
        public string GWADDRESSFORMAT { get; set; }
        public bool GWDEACTIVATED { get; set; }
        public string GWSTATE1 { get; set; }
        public string GWSTATE2 { get; set; }
        public string GWSTATE3 { get; set; }
        public bool GWDUPLICATECHECK { get; set; }
        public bool GWIDENTITYEXIST { get; set; }
        public bool GWISCOMPANY { get; set; }
        public bool GWISCONTACT { get; set; }
        public bool GWISEMPLOYEE { get; set; }
        public bool GWISEXTERNALEMPLOYEE { get; set; }
        public bool GWKEEPCONTACTSYNCHRON { get; set; }
        public string GWPHONETIC_CHRISTIANNAME { get; set; }
        public string GWPHONETIC_COMPNAME { get; set; }
        public string GWPHONETIC_NAME { get; set; }
        public bool GWSSERVICEPASSWORDSET { get; set; }
        public bool HDBLOCKEDFORSUPPORT { get; set; }
        public DateTime INSERTTIMESTAMP { get; set; }
        public string INSERTUSER { get; set; }
        public int INXMAIL_BOUNCES { get; set; }
        public string NAME { get; set; }
        public string OWNERGUID { get; set; }
        public string OWNERNAME { get; set; }
        public DateTime UPDATETIMESTAMP { get; set; }
        public string UPDATEUSER { get; set; }
    }
}