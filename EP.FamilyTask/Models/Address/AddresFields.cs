﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EP.Nikolina_Marina.Models.Address
{
    public class AddresFields
    {
        [DisplayName("Ime")]
        public string CHRISTIANNAME { get; set; }

        [DisplayName("Prezime")]
        public string NAME { get; set; }

        [DisplayName("Id")]
        [Key] public string GGUID { get; set; }

        [DisplayName("Ulica")]
        public string GWSTATE1 { get; set; }

        [DisplayName("Broj")]
        public string GWSTATE2 { get; set; }

        [DisplayName("Poštanski broj i Grad")]
        public string GWSTATE3 { get; set; }

        public string GWBRANCH { get; set; }
    }
}