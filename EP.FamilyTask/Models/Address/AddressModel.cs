﻿namespace EP.Nikolina_Marina.Models.Address
{
    public class AddressModel
    {
        public string ObjectType { get; set; }

        public string Id { get; set; }

        public AddresFields Fields { get; set; }

        public Links Links { get; set; }
    }
}
