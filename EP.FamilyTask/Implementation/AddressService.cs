﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using EP.Nikolina_Marina.Interfaces;
using EP.Nikolina_Marina.Models.Address;
using EP.Nikolina_Marina.Models.Address.Details;
using Newtonsoft.Json;
using RestSharp;

namespace EP.Nikolina_Marina.Implementation
{
    public class AddressService : IAddressService
    {
        private readonly IHttpClientFactory _factory;
        private readonly IMapper _mapper;

        public AddressService(IHttpClientFactory factory, IMapper mapper)
        {
            _factory = factory;
            _mapper = mapper;
        }

        public async Task<List<AddressModel>> GetMembersAsync()
        {
            var client = _factory.GetRestClient();

            var cancellationTokenSource = new CancellationTokenSource();

            var request = new RestRequest("/v3.0/type/ADDRESS/full", Method.GET, DataFormat.Json);

            var restResponse = await client.ExecuteAsync(request, cancellationTokenSource.Token);

            var response = JsonConvert.DeserializeObject<List<FullFamilyMemberModel>>(restResponse.Content);

            response = response.Where(x => x.Fields.GWBRANCH == "Trade").ToList();

            var result = new List<AddressModel>();

            _mapper.Map(response, result);

            return result;
        }

        public async Task<ExtendedAddreFields> GetMemberAsync(string id)
        {
            var client = _factory.GetRestClient();

            var cancellationTokenSource = new CancellationTokenSource();

            var request = new RestRequest($"/v3.0/type/ADDRESS/{id}", Method.GET, DataFormat.Json);

            var restResponse = await client.ExecuteAsync(request, cancellationTokenSource.Token);

            var response = JsonConvert.DeserializeObject<AddressModel>(restResponse.Content);

            var result = _mapper.Map<AddresFields, ExtendedAddreFields>(response.Fields);

            result.Etag = restResponse.Headers.FirstOrDefault(x => x.Name.ToUpperInvariant() == "ETAG")?.Value.ToString();

            return result;
        }

        public async Task UpdateMemberAsync(ExtendedAddreFields model)
        {
            model.GWBRANCH = "Trade";

            var client = _factory.GetRestClient();

            var cancellationTokenSource = new CancellationTokenSource();

            var request = new RestRequest($"/v3.0/type/ADDRESS/{model.GGUID}", Method.PUT, DataFormat.Json);

            request.AddHeader("If-Match", model.Etag);

            var body = _mapper.Map<ExtendedAddreFields, AddresFields>(model);

            var data = new { fields = body };

            var serialized = JsonConvert.SerializeObject(data);

            request.AddJsonBody(serialized);

            var restResponse = await client.ExecuteAsync(request, cancellationTokenSource.Token);
        }

        public async Task CreateMemberAsync(AddresFields model)
        {
            model.GWBRANCH = "Trade";

            var client = _factory.GetRestClient();

            var cancellationTokenSource = new CancellationTokenSource();

            var request = new RestRequest($"/v3.0/type/ADDRESS", Method.POST, DataFormat.Json);

            var data = new { fields = model };

            var serialized = JsonConvert.SerializeObject(data);

            request.AddJsonBody(serialized);

            var restResponse = await client.ExecuteAsync(request, cancellationTokenSource.Token);
        }

        public async Task DeleteMemberAsync(string id)
        {
            var client = _factory.GetRestClient();

            var cancellationTokenSource = new CancellationTokenSource();

            var request = new RestRequest($"/v3.0/type/ADDRESS/{id}", Method.DELETE, DataFormat.Json);

            await client.ExecuteAsync(request, cancellationTokenSource.Token);
        }
    }
}
