﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EP.Nikolina_Marina.Interfaces;
using EP.Nikolina_Marina.Models.Address;
using Microsoft.AspNetCore.Mvc;

namespace EP.Nikolina_Marina.Controllers
{
    public class AddressController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IAddressService _service;

        public AddressController(IAddressService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        // GET: FamilyMembers
        public async Task<IActionResult> Index()
        {
            var response = await _service.GetMembersAsync();

            return View(response.Select(x => x.Fields));
        }

        // GET: FamilyMembers/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null) return NotFound();

            var response = await _service.GetMemberAsync(id);

            if (response == null) return NotFound();

            return View(response);
        }

        // GET: FamilyMembers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: FamilyMembers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CHRISTIANNAME,NAME,GGUID,COMPNAME,GWSTATE1,GWSTATE2,GWSTATE3")]
            AddresFields addresFields)
        {
            if (ModelState.IsValid)
            {
                await _service.CreateMemberAsync(addresFields);
                return RedirectToAction(nameof(Index));
            }

            return View(addresFields);
        }

        // GET: FamilyMembers/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null) return NotFound();

            var response = await _service.GetMemberAsync(id);

            if (response == null) return NotFound();

            TempData["Etag"] = response.Etag;

            return View(response);
        }

        // POST: FamilyMembers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("CHRISTIANNAME,NAME,GGUID,COMPNAME,GWSTATE1,GWSTATE2,GWSTATE3")]
            AddresFields addresFields)
        {
            if (id != addresFields.GGUID) return NotFound();

            if (ModelState.IsValid)
            {
                var model = _mapper.Map<AddresFields, ExtendedAddreFields>(addresFields);
                model.Etag = TempData["Etag"].ToString();

                await _service.UpdateMemberAsync(model);

                return RedirectToAction(nameof(Index));
            }

            return View(addresFields);
        }

        // GET: FamilyMembers/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null) return NotFound();

            var response = await _service.GetMemberAsync(id);

            if (response == null) return NotFound();

            return View(response);
        }

        // POST: FamilyMembers/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            await _service.DeleteMemberAsync(id);

            return RedirectToAction(nameof(Index));
        }
    }
}