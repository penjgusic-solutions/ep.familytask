﻿using EP.Nikolina_Marina.Models.Address;
using EP.Nikolina_Marina.Models.Address.Details;

namespace EP.Nikolina_Marina.Profile
{
    public class FamilyProfile : AutoMapper.Profile
    {
        public FamilyProfile()
        {
            CreateMap<ExtendedAddreFields, AddresFields>().ReverseMap();

            CreateMap<FullFamilyMemberModel, AddressModel>();

            CreateMap<FamilyMemberDetailsFields, AddresFields>().ReverseMap();

        }
    }
}
